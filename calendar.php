<?php

include("lib/common.php");

$months = array
(
	1=>"January",
	2=>"February",
	3=>"March",
	4=>"April",
	5=>"May",
	6=>"June",
	7=>"July",
	8=>"August",
	9=>"September",
	10=>"October",
	11=>"November",
	12=>"December"
);

$now = getdate(time());
$year = $now['year'];
$month = $now['mon'];
$day = $now['mday'];

if((int)$_GET['month'])
{
	$month = (int)$_GET['month'];
	$day = 0;
}

$d = getdate(mktime(0, 0, 0, $month, 1, $year));
$i = 1 - $d['wday'];
$d = getdate(mktime(0, 0, 0, $month + 1, 0, $year));
$max = $d['mday'];

$users = Query("select birthday, id, name, displayname, sex, powerlevel from users where birthday != 0 order by name");
$cells = array();
while($user = Fetch($users))
{
	$d = getdate($user['birthday']);
	if($d['mon'] == $month)
	{
		$dd = $d['mday'];
		$age = $year - $d['year'];
		$cells[$dd] .= "<br />&bull; ".Userlink($user)."'s birthday (".$age.")\n";
	}
}

while($i <= $max)
{
	$grid .= format(
"
	<tr>
");
	for($dn = 0; $dn <= 6; $dn++)
	{
		$dd = $i + $dn;
		if($dd < 1 || $dd > $max)
			$label = "";
		else
			$label = format(
"
			{0}
			{1}", $dd, $cells[$dd]);
		$grid .= format(
"
		<td class=\"cell{0} smallFonts\" style=\"height: 80px; vertical-align: top\">
			{1}
		</td>
",	$cellClass, $label);
		$cellClass = ($cellClass+1) % 2;
	}
	$grid .= format(
"
	</tr>
");
	$i += 7;
}

$monthChoice = "";
for($i = 1; $i <= 12; $i++)
{
	if($i == $month)
	{
		$monthChoice .= format(
"
				<li>
					{0}
				</li>
", $months[$i]);
	}
	else
	{
		$monthChoice .= format(
"
				<li>
					<a href=\"calendar.php?month={0}\">{1}</a>
				</li>
", $i, $months[$i]);
	}
}

write(
"
<table class=\"outline margin\">
	<tr class=\"header0\">
		<th colspan=\"7\">
			{0} {1}
		</th>
	</tr>
	<tr class=\"header1\">
		<th {3}>Saturday</th>
		<th {3}>Monday</th>
		<th {3}>Tuesday</th>
		<th {3}>Wednesday</th>
		<th {3}>Thursday</th>
		<th {3}>Friday</th>
		<th {3}>Saturday</th>
	</tr>
	{2}
	<tr>
		<td class=\"cell2 smallFonts center\" colspan=\"7\">
			<ul class=\"pipemenu\">
				{4}
			</ul>
		</td>
	</tr>
</table>
",	$months[$month], $year, $grid, "style=\"width: 14.3%;\"", $monthChoice);

?>