<?php
include("lib/common.php");
$title = "Last known browsers";
$isMod = $loguser['powerlevel'] > 0;
$sort = "id asc";
$ual = "?";
if(isset($_GET['byua']))
{
	$sort = "lastknownbrowser asc";
	$ual .= "byua&amp;";
}

$numUsers = FetchResult("select count(*) from users", 0, 0);

$ppp = $loguser['postsperpage'];
if($ppp<1) $ppp=50;

if(isset($_GET['from']))
	$from = (int)$_GET['from'];
else
	$from = 0;

$peeps = Query("select id, name, displayname, lastip, lastknownbrowser, sex, powerlevel from users where powerlevel < 5 order by ".$sort." limit ".$from.", ".$ppp);

$numonpage = NumRows($peeps);
for($i = $ppp; $i < $numUsers; $i+=$ppp)
{
	if($i == $from)
		$pagelinks .= " ".(($i/$ppp)+1);
	else
		$pagelinks .= " <a href=\"lastknownbrowsers.php".$ual."from=".$i."\">".(($i/$ppp)+1)."</a>";
}
if($pagelinks)
{
	if($from == 0)
		$pagelinks = "1".$pagelinks;
	else
		$pagelinks = "<a href=\"lastknownbrowsers.php".$ual."\">1</a>".$pagelinks;
	Write("<div class=\"smallFonts pages\">Pages: {0}</div>", $pagelinks);
}


if($isMod)
	$format = "
		<tr class=\"cell{0}\">
			<td>
				{1}
			</td>
			<td>
				{2}
			</td>
			<td>
				{3}
			</td>
			<td>
				{4}
			</td>
			<td>
				{5}
			</td>
		</tr>
	";
else
	$format = "
		<tr class=\"cell{0}\">
			<td>
				{1}
			</td>
			<td>
				{2}
			</td>
			<td>
				{3}
			</td>
			<td>
				{5}
			</td>
		</tr>
	";

$items = "";
while($peep = Fetch($peeps))
{
	$lip = $peep['lastip'];
	$lkb = $peep['lastknownbrowser'];
	if(isset($_GET['showfull']))
		$lkb = str_replace("-->", "", str_replace("<!--", " &mdash;", $lkb));

	$cellClass = ($cellClass+1) % 2;
	$items .= format($format, $cellClass, $peep['id'], UserLink($peep), IP2C($lip), $lip, $lkb);
}

if($isMod)
	write("
	<table class=\"outline margin\">
		<tr class=\"header1\">
			<th>
				ID
			</th>
			<th>
				Name
			</th>
			<th>
				&nbsp;
			</th>
			<th>
				IP
			</th>
			<th>
				Last known browser
			</th>
		</tr>
		{0}
	</table>
", $items);
else
	write("
	<table class=\"outline margin\">
		<tr class=\"header1\">
			<th>
				ID
			</th>
			<th>
				Name
			</th>
			<th>
				&nbsp;
			</th>
			<th>
				Last known browser
			</th>
		</tr>
		{0}
	</table>
", $items);

?>