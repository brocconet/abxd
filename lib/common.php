<?php
//  AcmlmBoard XD support - Main hub

// SHUT THE FUCK UP
error_reporting(~E_ALL);

if(!is_file("lib/database.php"))
	die("You should <a href=\"install/index.php\">install</a> the board database first.");

include("settings.php");
include("snippets.php");
if($ajax)
	$overallTidy = 0;
//if($overallTidy)
	ob_start("DoFooter");

if(!isset($title))
	$title = "";

//WARNING: These things need to be kept in a certain order of execution.

include("mysql.php");
include("supersqlescape.php");
include("feedback.php");

$thisURL = $_SERVER['SCRIPT_NAME'];
if($q = $_SERVER['QUERY_STRING'])
	$thisURL .= "?$q";

include("loguser.php");

if(!isset($noViewCount))
	include("views.php");

include("tidy.php");
include("post.php");

include("themes/list.php");
$themeFiles = array_keys($themes);
$themeNames = array_values($themes);
$theme = $themeFiles[$loguser['scheme']];
if (!$loguserid) $theme = $defaultTheme;
$themeFile = "themes/$theme/style.css";
if(!file_exists($themeFile)) $themeFile = "themes/$theme/style.php";

if(file_exists("data/logos/logo.png")) $logopic = "data/logos/logo.png";
if(file_exists("data/logos/".$theme."_logo.png")) $logopic = "data/logos/".$theme."_logo.png";
else $logopic = "img/logo.png";

if(!isset($noAutoHeader))
	include("header.php");

function deSlashMagic($text)
{
		return $text; // legacy function, should be removed.
}

//Simple version -- may expand later.
function CheckTableBreaks($text)
{
	$text = strtolower($text);
	$openers = substr_count($text, "<table") + substr_count($text, "<div");
	$closers = substr_count($text, "</table>") + substr_count($text, "</div>");
	return ($openers != $closers);
}

function htmlval($text)
{
	//$text = str_replace("\"", "&quot;", $text);
	//$text = str_replace("<", "&lt;", $text);
	//return $text;
	return htmlentities($text, ENT_COMPAT, "UTF-8");
}

function filterPollColors($input)
{
/*
	$valid = "#0123456789ABCDEFabcdef";
	$output = "";
	for($i = 0; $i < strlen($input); $i++)
		if(strpos($valid, $input[$i]) !== FALSE)
			$output .= $input[$i];
	return $output;
*/
	return preg_replace("@[^#0123456789abcdef]@si", "", $input);
}

function RecalculateKarma($uid)
{
	$karma = 100;
	$karmaWeights = array(5, 10, 10, 15);
	$qKarma = "select powerlevel, up from uservotes left join users on id=voter where uid=".$uid." and powerlevel > -1";
	$rKarma = Query($qKarma);
	while($k = Fetch($rKarma))
	{
		if($k['up'])
			$karma += $karmaWeights[$k['powerlevel']];
		else
			$karma -= $karmaWeights[$k['powerlevel']];
	}
	Query("update users set karma=".$karma." where id=".$uid);
	return $karma;
}

function ParseThreadTags(&$title)
{
	preg_match_all("/\[(.*?)\]/", $title, $matches);
	foreach($matches[1] as $tag)
	{
		$title = str_replace("[".$tag."]", "", $title);
		$tag = strtolower($tag);
		
		//Start at a hue that makes "18" red.
		$hash = -105;
		for($i = 0; $i < strlen($tag); $i++)
			$hash += ord($tag[$i]);

		//That multiplier is only there to make "nsfw" and "18" the same color.
		$color = "hsl(".(($hash * 57) % 360).", 70%, 40%)";
		
		$tags .= "<span class=\"threadTag\" style=\"background-color: ".$color.";\">".$tag."</span>";
	}
	if($tags)
		$tags = " ".$tags;
	return $tags;
}

function cdate($format, $date = 0)
{
	global $loguser;
	if($date == 0)
		$date = time();
	$hours = (int)($loguser['timezone']/3600);
	$minutes = floor(abs($loguser['timezone']/60)%60);
	$plusOrMinus = $hours < 0 ? "" : "+";
	$timeOffset = $plusOrMinus.$hours." hours, ".$minutes." minutes";
	return gmdate($format, strtotime($timeOffset, $date));
}

include("write.php");

?>
