<?php
//  AcmlmBoard XD support - MySQL database wrapper functions

include("database.php");

$queries = 0;

$dblink = mysqli_connect($dbserv, $dbuser, $dbpass);
if (!$dblink) Kill("Could not connect to database.");
mysqli_select_db($dblink, $dbname) or Kill("Could not select database.");;
mysqli_set_charset($dblink, "utf8mb4"); // is this required?
mysqli_query($dblink, "SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci");
unset($dbpass);

function Query($query)
{
	global $dblink, $queries, $loguser, $thisURL;
	//write("#{0} - {1}<br/>", $queries, $query);
	$res = mysqli_query($dblink, $query);
	if (!$res) Kill(mysqli_error($dblink)."<br />Query was: <code>".$query."</code>");
	$queries++;
	return $res;
}

function Fetch($result)
{
	$res = mysqli_fetch_array($result);
	return $res;
}

function FetchResult($query, $row = 0, $field = 0)
{
	$res = Query($query);
	if(mysqli_num_rows($res) == 0) return -1;
	return mysqli_result($res, $row, $field);
}

function NumRows($result)
{
	return mysqli_num_rows($result);
}

// https://www.php.net/manual/en/class.mysqli-result.php#109782
function mysqli_result($res, $row, $field=0)
{
	$res->data_seek($row);
	$datarow = $res->fetch_array();
	return $datarow[$field];

}

function justEscape($text)
{
	global $dblink;
	return mysqli_real_escape_string($dblink, $text);
}

function insertID()
{
	global $dblink;
	return mysqli_insert_id($dblink);
}

?>
