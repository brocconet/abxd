<?php
//Generated and parsed by the Board Settings admin panel.

//Settings
$boardname = "ABXD";
$logoalt = "Another ABXD Board";
$logotitle = "Another ABXD Board";
$dateformat = "d.m.y, H:i:s";
$autoLockMonths = 12;
$warnMonths = 3;
$customTitleThreshold = 100;
$viewcountInterval = 10000;
$overallTidy = 0;
$theWord = "Arsehole";
$systemUser = 0;
$minWords = 3;
$minSeconds = 30;
$uploaderCap = 32;
$uploaderWhitelist = "png gif jpg jpeg txt css zip rar 7z ogg mp3 swf ogv mp4 webm svg";
$defaultTheme = "abxd20";

//Hacks
$hacks['forcetheme'] = -1;
$hacks['themenames'] = 0;
$hacks['postfilter'] = 0;

//Profile Preview Post
$profilePreviewText = "This is a sample post. It demonstrates various kinds of [i]markup[/i] and quotes and such.
This has to be one of the [b]longest[/b] preview messages ever seen in an AcmlmBoard.[quote=Barack Obama]I am Barack Obama and I approve this preview message.[quote=some guy]This is a nested quote. See what you get.[/quote][/quote]Well, what more could you [url=http://en.wikipedia.org]want to know[/url]? Perhaps how to do the classic infinite loop?
[source=qbasic]10 PRINT \"Hello World!\"
20 GOTO 10[/source]HEEEEY MACARONI! :D";

//Meta
$metaDescription = "The owner of this ABXD board was too lazy to edit the description field. Kill him.";
$metaKeywords = "abxd,acmlmboard";
?>