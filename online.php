<?php
//  AcmlmBoard XD - Realtime visitor statistics page
//  Access: all

include("lib/common.php");

$title = "Online users";

$time = (int)$_GET['time'];
if(!$time) $time = 300;

$qUsers  = "select * from users where lastactivity > ".(time()-$time)." order by lastactivity desc";
$qGuests = "select * from guests where date > ".(time()-$time)." and bot = 0 order by date desc";
$qBots   = "select * from guests where date > ".(time()-$time)." and bot = 1 order by date desc";
$rUsers = Query($qUsers);
$rGuests = Query($qGuests);
$rBots = Query($qBots);

$spans = array(60, 300, 900, 3600, 86400);
$spanList = "";
foreach($spans as $span)
{
	$spanList .= format(
"
			<li>
				<a href=\"online.php?time={0}\">{1}</a>
			</li>
",	$span, timeunits($span));
}
write(
"
	<div class=\"smallFonts\">
		Show visitors during the last
		<ul class=\"pipemenu\">
			{0}
		</ul>
	</div>
", $spanList);


$userList = "";
$i = 1;
if(NumRows($rUsers))
{
	while($user = Fetch($rUsers))
	{
		$cellClass = ($cellClass+1) % 2;
		if($user['lasturl'])
			$lastUrl = "<a href=\"".$user['lasturl']."\">".str_replace(array("%20","_"), " ", $user['lasturl'])."</a>";
		else
			$lastUrl = "None";

		$userList .= format(
	"
		<tr class=\"cell{0}\">
			<td>
				{1}
			</td>
			<td>
				{2}
			</td>
			<td>
				{3}
			</td>
			<td>
				{4}
			</td>
			<td>
				{5}
			</td>
			<td>
				{6} {7}
			</td>
		</tr>
	",	$cellClass, $i, UserLink($user), cdate("d-m-y G:i:s", $user['lastactivity']),
		($user['lastposttime'] ? cdate("d-m-y G:i:s",$user['lastposttime']) : "Never"),
		$lastUrl, $user['lastip'], IP2C($user['lastip']));
		$i++;
	}
}
else
	$userList = "<tr class=\"cell0\"><td colspan=\"6\">No users</td></tr>";

$guestList = "";
if(NumRows($rGuests))
{
	$i = 1;
	while($guest = Fetch($rGuests))
	{
		$cellClass = ($cellClass+1) % 2;
		if($guest['date'])
			$lastUrl = "<a href=\"".$guest['lasturl']."\">".str_replace(array("%20","_"), " ", $guest['lasturl'])."</a>";
		else
			$lastUrl = "None";
		
		$guestList .= format(
"
		<tr class=\"cell{0}\">
			<td>{1}</td>
			<td title=\"{2}\">{3}</td>
			<td>{4}</td>
			<td>{5}</td>
			<td>{6} {7}</td>
		</tr>
",	$cellClass, $i, htmlspecialchars($guest['useragent']),
	htmlspecialchars(substr($guest['useragent'], 0, 65)), cdate("d-m-y G:i:s", $guest['date']),
	$lastUrl, $guest['ip'], IP2C($guest['ip']));
		$i++;
	}
}
else
	$guestList = "<tr class=\"cell0\"><td colspan=\"5\">No guests</td></tr>";

$botList = "";
if(NumRows($rBots))
{
	$i = 1;
	while($bot = Fetch($rBots))
	{
		$cellClass = ($cellClass+1) % 2;
		if($bot['date'])
			$lastUrl = "<a href=\"".$bot['lasturl']."\">".str_replace(array("%20","_"), " ", $bot['lasturl'])."</a>";
		else
			$lastUrl = "None";

		$botList .= format(
"
		<tr class=\"cell{0}\">
			<td>{1}</td>
			<td title=\"{2}\">{3}</td>
			<td>{4}</td>
			<td>{5}</td>
			<td>{6}</td>
		</tr>
",	$cellClass, $i, htmlspecialchars($bot['useragent']),
	htmlspecialchars(substr($bot['useragent'], 0, 65)), cdate("d-m-y G:i:s", $bot['date']),
	$lastUrl, $bot['ip']);
		$i++;
	}
} else
	$botList = "<tr class=\"cell0\"><td colspan=\"5\">No bots</td></tr>";

write(
"
	<table class=\"outline margin\">
		<tr class=\"header0\">
			<th colspan=\"6\">
				Online users
			</th>
		</tr>
		<tr class=\"header1\">
			<th style=\"width: 30px;\">
				#
			</th>
			<th>
				Name
			</th>
			<th style=\"width: 140px;\">
				Last view
			</th>
			<th style=\"width: 140px;\">
				Last post
			</th>
			<th>
				URL
			</th>
			<th style=\"width: 140px;\">
				IP
			</th>
		</tr>
		{0}
	</table>
	<table class=\"outline margin\">
		<tr class=\"header1\">
			<th style=\"width: 30px;\">
				#
			</th>
			<th>
				User agent
			</th>
			<th style=\"width: 140px;\">
				Last view
			</th>
			<th>
				URL
			</th>
			<th style=\"width: 140px;\">
				IP
			</th>
		</tr>
		<tr class=\"header0\">
			<th colspan=\"5\">
				Guests
			</th>
		</tr>
		{1}
		<tr class=\"header0\">
			<th colspan=\"5\">
				Bots
			</th>
		</tr>
		{2}
	</table>
", $userList, $guestList, $botList);

?>
